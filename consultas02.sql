USE ejercicio01;

-- vaciar la tabla persona
-- TRUNCATE persona;
-- realizar consulta a tabla persona
SELECT * FROM persona;

-- contar totalidad de registros en la tabla persona
SELECT COUNT(*) AS 'Total de registros' FROM persona;

-- seleccionar a todas personas que son del Estado de Veracruz
SELECT * FROM persona WHERE direccion LIKE '%Veracruz';
-- Listar personas nacidas en el año 2000
SELECT * FROM persona WHERE fechaNacimiento LIKE '%2000%';
-- Listar nombre completo de las personas registradas
SELECT CONCAT(nombre, ' ', apePat, ' ', apeMat) 
	AS 'Nombre completo' FROM persona;
-- Listar nombres que comiencen con letra A
SELECT nombre FROM persona WHERE nombre LIKE 'A%';

