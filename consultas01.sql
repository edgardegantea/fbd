-- Ingresar y usar la base de datos ejercicio01
USE ejercicio01;

-- Seleccionar todo de la tabla persona
SELECT * FROM persona;

-- Insertar datos
INSERT INTO persona(nombre, apePat, apeMat, fechaNacimiento, email, direccion, telefono)
VALUES ('Luis', 'Degante', 'Aguilar', '2000/03/02', 'luis@mail.com', 'Tehuacán, Puebla', '2311234567'),
	('Ana', 'López', 'Gutiérrez', '1999/05/23', 'ana@mail.com', 'Guerrero, Guerrero', '2356987410'),
	('Antonio', 'Lucas', 'Juárez', '2000/03/10', 'antonio@mail.com', 'Tuxtla Gutiérrez, Chiapas', '2223335556'),
	('Adriana', 'Cuevas', 'de la Merced', '1995/01/22', 'adriana@mail.com', 'Atzalan, Veracruz', '5542253214'),
	('Juan', 'López', 'Gutiérrez', '1999/05/24', 'jhony@mail.com', 'Guerrero, Guerrero', '2356987410'),
	('Pedro', 'Lucas', 'Juárez', '2000/03/23', 'peterparker@mail.com', 'Tuxtla Gutiérrez, Chiapas', '2223335556'),
	('Karely', 'Cuevas', 'de la Merced', '1995/06/10', 'karely@mail.com', 'Atzalan, Veracruz', '5542253214'),
	('Suheidy', 'López', 'Gutiérrez', '1999/08/20', 'suhe@mail.com', 'Guerrero, Guerrero', '2356987410'),
	('Nerina', 'Lucas', 'Juárez', '2000/09/10', 'nerina@mail.com', 'Tuxtla Gutiérrez, Chiapas', '2223335556'),
	('Monica', 'Cuevas', 'de la Merced', '1995/11/22', 'moni@mail.com', 'Atzalan, Veracruz', '5542253214'),
	('Monse', 'Vázquez', 'Ventura', '1999/12/30', 'monse@mail.com', 'Xalapa, Veracruz', '2330212256'),
	('Hugo', 'Lucas', 'Juárez', '2000/05/10', 'hugo@mail.com', 'Jalacingo, Veracruz', '2223335556'),
	('Fernando', 'Cuevas', 'de la Merced', '1995/11/22', 'fernand@mail.com', 'Xiutetelco, Puebla', '5542253214'),
	('Marco', 'López', 'Gutiérrez', '1999/04/23', 'marco@mail.com', 'Altotonga, Veracruz', '2356987410'),
	('Leticia', 'Lucas', 'Juárez', '2000/03/19', 'leticia@mail.com', 'Hueytamalco, Puebla', '2223335556'),
	('Briseida', 'Cuevas', 'de la Merced', '1995/09/22', 'briseida@mail.com', 'Tlapacoyan, Veracruz', '5542253214'),
	('Alan', 'Muñoz', 'Cárdenas', '1987/03/30', 'alan@mail.com', 'Misantla, Veracruz', '2330212256');