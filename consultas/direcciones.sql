CREATE DATABASE ejercicio01;
USE ejercicio01;

CREATE TABLE estado(
	claveEstado VARCHAR(5) NOT NULL,
	municipio VARCHAR(150) NOT NULL,
    PRIMARY KEY(claveEstado)
);

CREATE TABLE municipio(
	claveMunicipio VARCHAR(5) NOT NULL,
	municipio VARCHAR(150) NOT NULL,
    claveEstado VARCHAR(5) NOT NULL,
    INDEX(claveEstado),
    FOREIGN KEY(claveEstado) REFERENCES estado(claveEstado),
    PRIMARY KEY(claveMunicipio)
);

CREATE TABLE poblacion(
	clavePoblacion VARCHAR(5) NOT NULL,
	poblacion VARCHAR(150) NOT NULL,
    claveMunicipio VARCHAR(5) NOT NULL,
    INDEX(claveMunicipio),
    FOREIGN KEY(claveMunicipio) REFERENCES municipio(claveMunicipio),
    PRIMARY KEY(clavePoblacion)
);

CREATE TABLE direccion(
	id INT NOT NULL AUTO_INCREMENT,
	calle VARCHAR(100) NOT NULL,
    numeroInterior VARCHAR(5) NULL,
    numeroExterior INT NULL,
	clavePoblacion VARCHAR(5) NOT NULL,
    INDEX(clavePoblacion),
    FOREIGN KEY(clavePoblacion) REFERENCES poblacion(clavePoblacion),
    PRIMARY KEY(id)
);

CREATE TABLE persona(
    id INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apePat VARCHAR(50) NOT NULL,
    apeMat VARCHAR(50) NOT NULL,
    fechaNacimiento DATE NOT NULL,
    email VARCHAR(80) NULL,
    idDireccion INT NOT NULL,
    INDEX(idDireccion),
    FOREIGN KEY(idDireccion) REFERENCES direccion(id),
    telefono VARCHAR(15) NOT NULL,
    PRIMARY KEY(id)
);
