CREATE DATABASE ejercicio01;
USE ejercicio01;

CREATE TABLE persona(
    id INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apePat VARCHAR(50) NOT NULL,
    apeMat VARCHAR(50) NOT NULL,
    fechaNacimiento DATE NOT NULL,
    email VARCHAR(80) NULL,
    direccion VARCHAR(120) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    PRIMARY KEY(id)
);